package com.group.masterdata.Controller;

import com.group.masterdata.Model.Company;
import com.group.masterdata.Service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping(value = {"/", "/index"})
    public ModelAndView listAllCompanies() {
        ModelAndView model = new ModelAndView("index");
        List<Company> companyList = companyService.listAllCompanies();
        model.addObject("companyList", companyList);
        return model;
    }

    @GetMapping("/companyform")
    public ModelAndView addCompany() {
        ModelAndView model = new ModelAndView("company_form");

        List<String> countries = companyService.listAllCountries();
        model.addObject("countries", countries);

        /*Company parent = companyService.listCompaniesHierarchical();
        model.addObject("parent", parent);*/

        List<Company> companies = companyService.listAllCompanies();
        model.addObject("companies", companies);

        Company company = new Company();
        model.addObject("companyForm", company);

        return model;
    }

    @PostMapping("/save")
    public ModelAndView addCompany(@ModelAttribute("companyForm") Company company, @RequestParam(value = "action") String action, Model view) {
        ModelAndView model = new ModelAndView("company_form");
        if (action.equals("save")) {
            boolean checkAdded = companyService.addCompany(company);
            if (checkAdded)
                view.addAttribute("success", "Gesellschaft wurde erfolgreich hinzugefügt!");
            else
                view.addAttribute("error", "Gesellschaft kann nicht hinzugefügt werden!");
        }
        if (action.equals("cancel"))
            return listAllCompanies();
        return model;
    }
}
