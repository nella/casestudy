package com.group.masterdata.Service;

import com.group.masterdata.DAO.CompanyDAO;
import com.group.masterdata.Model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyDAO companyDAO;

    @Override
    public List<Company> listAllCompanies() {
        return companyDAO.listAllCompanies();
    }

    @Override
    public boolean addCompany(Company company) {
        return companyDAO.addCompany(company);
    }

    @Override
    public List<String> listAllCountries() {
        return companyDAO.listAllCountries();
    }

    @Override
    public Company listCompaniesHierarchical() {
        return companyDAO.listCompaniesHierarchical();
    }
}
