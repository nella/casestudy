package com.group.masterdata.Service;

import com.group.masterdata.Model.Company;

import java.util.List;

public interface CompanyService {

    public List<Company> listAllCompanies();

    public boolean addCompany(Company company);

    public List<String> listAllCountries();

    public Company listCompaniesHierarchical();
}
