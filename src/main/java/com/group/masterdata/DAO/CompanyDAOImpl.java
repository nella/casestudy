package com.group.masterdata.DAO;

import com.group.masterdata.Model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CompanyDAOImpl implements CompanyDAO {

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Company> listAllCompanies() {
        String sql = "SELECT * FROM company ORDER BY name";
        return jdbcTemplate.query(sql, new CompanyMapper());
    }

    @Override
    public boolean addCompany(Company company) {
        String sql = "INSERT INTO company VALUES (?, ?, ?, ?)";
        try {
            jdbcTemplate.update(sql, new Object[]{company.getPartnerNr(), company.getName(), company.getCountry(), company.getParentId()});
        } catch (DataIntegrityViolationException e) {
            return false;
        }
        return true;
    }

    @Override
    public List<String> listAllCountries() {
        String sql = "SELECT DISTINCT country FROM company";
        return jdbcTemplate.query(sql, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("country");
            }
        });
    }

    @Override
    public Company listCompaniesHierarchical() {
        String searchParentSql = "SELECT * FROM company WHERE parentid IS NULL";
        Company parent = jdbcTemplate.queryForObject(searchParentSql, new CompanyMapper());

        String searchChildrenSql = "SELECT * FROM company WHERE parentid = ?";
        parent.setSubcompanies(jdbcTemplate.query(searchChildrenSql, new Object[]{parent.getPartnerNr()}, new CompanyMapper()));

        for (Company company : parent.getSubcompanies())
            company.setSubcompanies(jdbcTemplate.query(searchChildrenSql, new Object[]{company.getPartnerNr()}, new CompanyMapper()));
        return parent;
    }

    private static final class CompanyMapper implements RowMapper<Company> {
        @Override
        public Company mapRow(ResultSet rs, int rowNum) throws SQLException {
            Company company = new Company();
            company.setPartnerNr(rs.getString("partnernr"));
            company.setName(rs.getString("name"));
            company.setCountry(rs.getString("country"));
            company.setParentId(rs.getString("parentid"));
            return company;
        }
    }

}
