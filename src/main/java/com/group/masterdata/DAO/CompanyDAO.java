package com.group.masterdata.DAO;

import com.group.masterdata.Model.Company;

import java.util.List;

public interface CompanyDAO {

    public List<Company> listAllCompanies();

    public boolean addCompany(Company company);

    public List<String> listAllCountries();

    public Company listCompaniesHierarchical();
}
