package com.group.masterdata.Model;

import java.util.List;

public class Company {

    private String partnerNr;
    private String name;
    private String country;
    private String parentId;
    private List<Company> subcompanies;

    public String getPartnerNr() {
        return partnerNr;
    }

    public void setPartnerNr(String partnerNr) {
        this.partnerNr = partnerNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<Company> getSubcompanies() {
        return subcompanies;
    }

    public void setSubcompanies(List<Company> subcompanies) {
        this.subcompanies = subcompanies;
    }
}
