<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Stammdatenpflege</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
</head>
<body>
<div class="jumbotron jumbotron-fluid text-center">
    <h1>Willkommen auf ABC Konzern!</h1>
</div>
<div class="container">
    <div class="card">
        <h4 class="card-header">Gesellschaft hinzufügen</h4>
        <div class="card-body">
            <spring:url value="/add" var="add"/>
            <form:form modelAttribute="companyForm" method="post" action="${add }">
                <table class="table table-bordered">
                    <tr>
                        <td>Gesellschaftsname:</td>
                        <td><form:input path="name"/></td>
                    </tr>
                    <tr>
                        <td>Land:</td>
                        <td><form:input path="country"/></td>
                    </tr>
                    <tr>
                        <td>Partner-Nr.:</td>
                        <td><form:input path="partnerNr"/></td>
                    </tr>
                    <tr>
                        <td>Mutter:</td>
                        <td><form:input path="parentId"/></td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-info" style="margin-bottom: 10px">Gesellschaft hinzufügen</button>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
