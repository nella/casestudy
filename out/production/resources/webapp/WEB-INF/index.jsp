<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Stammdatenpflege</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"/>
</head>
<body>
<div class="jumbotron jumbotron-fluid text-center">
    <h1>Willkommen auf ABC Konzern!</h1>
</div>
<div class="container">
    <div class="card">
        <h4 class="card-header">Alle unsere Tochtergesellschaften</h4>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Gesellschaftsname</th>
                    <th>Land</th>
                    <th>Partner-Nr.</th>
                    <th>Mutter</th>
                </tr>
                <c:forEach items="{companyList }" var="company">
                    <tr>
                        <td ${company.partnerNr }></td>
                        <td ${company.name }></td>
                        <td ${company.country }></td>
                        <td ${company.parentId }></td>
                    </tr>
                </c:forEach>
            </table>
            <h4 class="text-danger" th:if="${error}" th:text="${error}"></h4>
            <h4 class="text-danger" th:if="${err}" th:text="${err}"></h4>
            <h4 class="text-success" th:if="${succ}" th:text="${succ}"></h4>
            <spring:url value="/add" var="add">
                <button type="submit" class="btn btn-info" style="margin-bottom: 10px">Gesellschaft hinzufügen</button>
            </spring:url>
        </div>
    </div>
</div>
</body>
</html>
